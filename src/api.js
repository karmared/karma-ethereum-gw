const jayson = require('jayson/promise');
const config = require(__dirname + '/../config/config.json');
const _ = require('lodash');
const logger = require('../src/logger')(require('path').basename(__filename));
const BigNumber = require('bignumber.js');

const models = require('../models');
const Op = models.Sequelize.Op;


class ApiServer {
    constructor() {
        this._ethereum = null;
        this._exchange = null;

        this.server = jayson.server({

            generate_address: (args) => { return this.generateAddress(args) },
            info: (args) => { return this.info(args) },
            is_exist: (args) => { return this.isExist(args) },
            rejection: function (args) {
                return new Promise(function (resolve, reject) {
                    // server.error just returns {code: 501, message: 'not implemented'}
                    reject(server.error(501, 'not implemented'));
                });
            }
        });
    }

    setEthereum(ethereum) {
        this._ethereum = ethereum;
    }

    setExchange(exchange) {
        this._exchange = exchange;
    }

    generateAddress(args) {

        let self = this;
        return new Promise(async function(resolve, reject) {

            if (!args || args.length !== 2) {
                reject({code: 403, message: 'bad params'});
            }

            let accountId = null;
            try {
                let type = args[0];
                if (type !== 'ETH') {
                    reject({code: 403, message: 'unsupported type'});
                    return
                }


                let name = args[1];
                let id = await self._exchange.getAccountId(name);
                accountId = Number(id);
                if (isNaN(accountId) || accountId === 0) throw new Error('NaN');
            } catch (e) {
                return reject({code: 403, message: 'invalid account'});
            }

            return models.Addresses.findOne({where: {accountId: accountId}}).then(record => {
                if (record) {
                    return resolve({address: record.address});
                }

                let address = self._ethereum.generateAddress(accountId);

                return models.Addresses.create({
                    accountId: accountId,
                    address: address.address
                }).then((record) => {
                    self._ethereum.addMonitAddress(record.address);
                    return resolve({address: record.address});
                });
            }).catch(e => {
                logger.error('Error gen address:', e);
                return reject({code: 500, message: 'server error'});
            });
        })
    }

    isExist(args) {

        let self = this;
        return new Promise(async function(resolve, reject) {

            if (!args || args.length !== 2) {
                reject({code: 403, message: 'bad params'});
            }

            let accountId = null;
            try {
                let type = args[0];
                if (type !== 'ETH') {
                    reject({code: 403, message: 'unsupported type'});
                    return
                }


                let name = args[1];
                let id = await self._exchange.getAccountId(name);
                accountId = Number(id);
                if (isNaN(accountId) || accountId === 0) throw new Error('NaN');
            } catch (e) {
                return reject({code: 403, message: 'invalid account'});
            }

            return models.Addresses.findOne({where: {accountId: accountId}}).then(record => {
                return resolve({"exists": record ? true : false});
            }).catch(e => {
                logger.error('Error gen address:', e);
                return reject({code: 500, message: 'server error'});
            });
        })
    }

    info(args) {

        let self = this;
        return new Promise(async function(resolve, reject) {

            if (!args || args.length !== 1) {
                reject({code: 403, message: 'bad params'});
            }

            let type = args[0];
            if (type !== 'ETH') {
                let type = args[0];
                reject({code: 403, message: 'unsupported type'});
                return
            }
            let fee = self._ethereum.fee();

            resolve({
                type: "ETH",
                min: config.tokens["ETH"].min,
                fee: fee.shiftedBy(-config.tokens["ETH"].precision).toFixed()
            });
        })
    }

    start() {
        this.server.http().listen(config.port);
    }
}

module.exports = ApiServer;