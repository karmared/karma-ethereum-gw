const winston = require('winston');

module.exports = function (label) {
    var logger = new (winston.Logger)({
        level: 'debug',
        transports: [
            new (winston.transports.Console)({
                timestamp: function () {
                    return new Date().toISOString();
                },
                formatter: function (options) {
                    // Return string will be passed to logger.
                    return options.timestamp() + ' ' + options.level.toUpperCase() + ' ' + label + ' ' + (options.message ? options.message : '') +
                        (options.meta && Object.keys(options.meta).length ? '\n\t' + JSON.stringify(options.meta) : '' );
                }
            })
        ]
    });

    return logger;
};