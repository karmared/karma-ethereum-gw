const logger = require('../src/logger')(require('path').basename(__filename));
const BigNumber = require('bignumber.js');
const Bluebird = require('bluebird');

const bip39 = require('bip39');
const bitcoin = require('bitcoinjs-lib');
const hdkey = require('ethereumjs-wallet/hdkey');
const keythereum = require("keythereum");
const ethereumWallet = require('ethereumjs-wallet');

const Web3 = require('web3');
const Tx = require('ethereumjs-tx');

const util = require('ethereumjs-util');

const rp = require('request-promise');

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

class Wallet {

    constructor() {

        this._wallet = {
            main: null,
            deposit: null
        };

        this._address = null;
        this._monitAddresses = new Set();
        this._monitContracts = new Set();

        this._maxGasPrice = new BigNumber("2000000000"); // 2 GWEI by default
        this._minGasPrice = new BigNumber("1000000000"); // 2 GWEI by default
        this._gasPrice = new BigNumber("2000000000"); // 2 GWEI by default
        this._confirmations = 30;

        this._tokens = {};

        this._web3 = new Web3();
        //this._web3.setProvider(new this._web3.providers.HttpProvider(this._address));

        this._stopMonitoring = null;

        this._handlers = {
            'newIn': [],
            'lastBlock': []
        };
    }

    on(eventName, callback) {
        if (this._handlers.hasOwnProperty(eventName)) {
            this._handlers[eventName].push(callback);
        }
    }

    off(eventName, handle) {
        if (this._handlers.hasOwnProperty(eventName)) {
            let handleIndex = this._handlers[eventName].indexOf(handle);
            if (handleIndex >= 0) {
                this._handlers[eventName].splice(handleIndex, 1);
            }
        }
    }

    emit() {
        let p = [];
        let args = Array.prototype.slice.call(arguments);
        let eventName = args.shift();
        if (this._handlers.hasOwnProperty(eventName)) {
            for (let i = 0, length = this._handlers[eventName].length; i < length; i++)
                p.push(this._handlers[eventName][i].apply(undefined, args));
        }
        return p;
    }

    async connect() {
        try {
            this._web3.setProvider(new this._web3.providers.HttpProvider(this._address));

            await this.updateGasPrice();
            setInterval(() => { this.updateGasPrice() }, 300*1000);

            logger.info('connected', this._address);
        } catch (e) {
            logger.error(`Cannot connect to ${this._address}: ${e}`);
            throw e;
        }
    }

    setConfig(config) {
        this._address = config.api;

        // HOT
        let privateKey = new Buffer(config.main.private_key, 'hex');
        this._wallet.main = ethereumWallet.fromPrivateKey(privateKey);

        // Deposit
        let seed = bip39.mnemonicToSeed(config.deposit.seed);
        this._wallet.deposit = bitcoin.HDNode.fromSeedBuffer(seed);

        if (config.max_gas_price)
            this._maxGasPrice = new BigNumber(config.max_gas_price);

        if (config.min_gas_price)
            this._minGasPrice = new BigNumber(config.min_gas_price);

        if (config.confirmations && Number(config.confirmations) > 0)
            this._confirmations = Number(config.confirmations);
    }

    setMonitAddresses(addresses) {
        this._monitAddresses = new Set(addresses);
    }

    addMonitAddress(address) {
        this._monitAddresses.add(address);
    }

    getDepositWallet(accountId) {
        let path = `m/44'/60'/${accountId}'/0`;
        let child = this._wallet.deposit.derivePath(path);

        let w0 = hdkey.fromExtendedKey(child.deriveHardened(0).toBase58()).getWallet();
        return w0;
    }

    generateAddress(accountId) {
        let w0 = this.getDepositWallet(accountId);

        return {
            address: w0.getAddressString(),
            privateKey: w0.getPrivateKey(),
            publicKey: w0.getPublicKey()
        }
    }

    async startMonitoring(start) {

        this._stopMonitoring = false;

        let _start = start;
        let _lastBlock = this._web3.eth.blockNumber;
        if (!_start || isNaN(_start)) _start = _lastBlock;

        while (true) {
            let lastSafeBlock = 0;
            try {
                lastSafeBlock = this._web3.eth.blockNumber - this._confirmations;
                logger.verbose(`scan ${_start} - ${lastSafeBlock}`);
            } catch (e) {
                logger.error("Cannot get block number");
            }

            try {
                for (let blockNumber = _start; blockNumber < lastSafeBlock; blockNumber++) {
                    logger.info('Block:', blockNumber);
                    let block = await Bluebird.promisify(this._web3.eth.getBlock)(blockNumber).timeout(3000);

                    for(var j = 0; j < block.transactions.length; j++) {
                        let tx = await Bluebird.promisify(this._web3.eth.getTransactionFromBlock)(blockNumber, j).timeout(3000);

                        // ETH
                        if (tx.to && this._monitAddresses.has(tx.to)) {

                            let payment = {
                                txId: tx.hash,
                                blockNumber: tx.blockNumber,
                                from: tx.from,
                                to: tx.to,
                                amount: tx.value.toString(),
                                asset: 'ETH',
                                raw: tx,
                                error: null
                            };

                            await Promise.all(this.emit('newIn', payment));
                        }

                    }

                    await Promise.all(this.emit('lastBlock', blockNumber));
                    _start = lastSafeBlock;
                }
            } catch (e) {
                logger.error('Error proccess transactions:', e);
            }

            if (this._stopMonitoring) break;

            await sleep(15 * 1000);
        }
    }

    sendRawTransaction(tx) {
        return new Promise((resolve, reject) => {
            try {
                this._web3.eth.sendRawTransaction('0x' + tx.toString('hex'), function(err, hash) {
                    if (!err) {
                        logger.info(`Transaction sent ${hash}`);
                        resolve(hash);
                    }
                    else {
                        logger.error("Cannot send tx:", err)
                        reject(err);
                    }
                });
            } catch (e) {
                logger.error("Cannot execute sendRawTransaction:", e);
                reject(e);
            }
        });
    }

    async balance(address) {
        let balance = await Bluebird.promisify(this._web3.eth.getBalance)(address);
        return balance.toString();
    }

    async hotBalance() {
        return this.balance(this._wallet.main.getAddressString());
    }

    gasPrice() {
        return this._gasPrice;
    }

    gasLimit() {
        return 21000;
    }

    fee() {
        return this.gasPrice().multipliedBy(2*this.gasLimit());
    }

    async sendEthFromMain(to, amount) {

        let nonce = this._web3.eth.getTransactionCount(this._wallet.main.getAddressString(), "pending");
        let nonceHex = this._web3.toHex(nonce);

        let gasPrice = this.gasPrice();
        let gasPriceHex = this._web3.toHex(Number(gasPrice.toFixed(0)));
        let gasLimitHex = this._web3.toHex(this.gasLimit());

        let rawTx = {
            nonce: nonceHex,
            gasPrice: gasPriceHex,
            gasLimit: gasLimitHex,
            to: to,
            value: Number(amount)
        };

        console.log('raw:', rawTx);

        let tx = new Tx(rawTx);
        tx.sign(this._wallet.main.getPrivateKey());

        let serializedTx = tx.serialize();
        let txId = await this.sendRawTransaction(serializedTx);

        return txId;
    }

    async sendEthToMain(accountId, amount) {

        let fromW = this.getDepositWallet(accountId);

        let nonce = this._web3.eth.getTransactionCount(fromW.getAddressString(), "pending");
        let nonceHex = this._web3.toHex(nonce);

        let gasPrice = this.gasPrice();
        let gasPriceHex = this._web3.toHex(Number(gasPrice.toFixed(0)));
        let gasLimitHex = this._web3.toHex(this.gasLimit());

        let _amount = new BigNumber(amount).minus(gasPrice*this.gasLimit());

        let rawTx = {
            nonce: nonceHex,
            gasPrice: gasPriceHex,
            gasLimit: gasLimitHex,
            to: this._wallet.main.getAddressString(),
            value: Number(_amount.toFixed(0))
        };

        console.log('raw:', rawTx);

        let tx = new Tx(rawTx);
        tx.sign(fromW.getPrivateKey());

        let serializedTx = tx.serialize();
        let txId = await this.sendRawTransaction(serializedTx);
        return txId;
    }

    stopMonitoring() {
        this._stopMonitoring = true;
    }

    isAddress(address) {
        if (!/^(0x)?[0-9a-f]{40}$/i.test(address)) {
            // check if it has the basic requirements of an address
            return false;
        } else if (/^(0x)?[0-9a-f]{40}$/.test(address) || /^(0x)?[0-9A-F]{40}$/.test(address)) {
            // If it's all small caps or all all caps, return true
            return true;
        } else {
            // Otherwise check each case
            return util.isValidChecksumAddress(address);
        }
    }

    async updateGasPrice() {

        let options = {
            uri: 'https://ethgasstation.info/json/ethgasAPI.json',
            method: 'GET',
            json: true,
            headers: {
                'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36',
                'authority': 'ethgasstation.info'
            }
        };

        let price = null;
        try {
            // price will be returned and GWEI/10
            const result = await rp(options);
            console.log("gas station:", result);
            price = new BigNumber(result.average);
            price = price.multipliedBy(100000000); // multiply to 10^9/10
        } catch (e) {
            logger.warn("Cannot update gas price from ethgasstation");
            try {
                price = new BigNumber(this._web3.eth.gasPrice.toFixed());
            } catch (e) {
                logger.warn("Cannot update gas price from node too");
                throw e;
            }
        }

        console.log("P:", price);

        if (price.isLessThan(this._minGasPrice)) {
            logger.warn("Price less than min price from config");
            price = this._minGasPrice;
        } else if (price.isGreaterThan(this._maxGasPrice)) {
            logger.warn("Price less than min price from config");
            price = this._maxGasPrice;
        }
        this._gasPrice = price;
    }
}

module.exports = Wallet;
