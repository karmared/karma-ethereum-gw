'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('payment_requests', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            requestId: {
                allowNull: false,
                field: 'request_id',
                unique: true,
                type: Sequelize.STRING
            },
            to: {
                allowNull: false,
                type: Sequelize.STRING
            },
            amount: {
                allowNull: false,
                type: Sequelize.STRING
            },
            asset: {
                allowNull: false,
                type: Sequelize.STRING
            },
            status: {
                type: Sequelize.STRING,
                allowNull: false,
                defaultValue: 'NEW'
            },
            error: {
                type: Sequelize.TEXT,
            },
            txId: {
                field: 'tx_id',
                type: Sequelize.STRING,
            },
            txRaw: {
                field: 'tx_raw',
                type: Sequelize.TEXT,
            },
            createdAt: {
                field: 'created_at',
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                field: 'updated_at',
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('PaymentRequests');
    }
};