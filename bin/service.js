const config = require(__dirname + '/../config/config.json');

const ApiServer = require(__dirname + '/../src/api');
const Exchange = require('../src/bitshares');
const Ethereum = require('../src/ethereum');
const BigNumber = require('bignumber.js');

const models = require('../models');
const Op = models.Sequelize.Op;

const logger = require('../src/logger')(require('path').basename(__filename));

logger.info('Started');

var lastBlock = null;
var lastOp = null;
let exchange = null;
let ethereum = null;

var tokens = config.tokens;
var exchangeTokens = {};

for (let name in tokens) {
    if (tokens.hasOwnProperty(name)) {
        exchangeTokens[tokens[name].exchangeId] = name;
    }
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

const startApiServer = async function () {
    apiServer = new ApiServer();
    apiServer.setEthereum(ethereum);
    apiServer.setExchange(exchange);
    apiServer.start();
    console.log('API server started');
};

const processWithdraw = async function (payment) {
    console.log(`New withdraw to ${payment.address} (txid: ${payment.id})`);

    let exchangeId = payment.asset;
    if (!exchangeTokens.hasOwnProperty(exchangeId)) {
        logger.error(`Withdrawing asset ${exchangeId} is not supported`);
        return;
    }
    let token = tokens[exchangeTokens[exchangeId]];
    let amount = new BigNumber(payment.amount);
    amount = amount.shiftedBy(-token.exchangePrecision);


    let record = null;
    let created = null;
    try {
        let status = 'NEW';
        if (amount.isLessThan(token.min)) {
            logger.error(`Withdraw less than min: ${payment.id}`);
            status = 'SMALL';
        }

        [record, created] = await models.Withdraws.findOrCreate({
            where: {txId: payment.id}, defaults: {
                txId: payment.id,
                from: payment.from,
                to: payment.to,
                amount: payment.amount,
                asset: payment.asset,
                memo: payment.memo,
                blockNumber: payment.blockNumber,
                status: status,
                raw: JSON.stringify(payment.raw)
            }
        });
    } catch (e) {
        logger.error(`Cannot save withdraw op #${payment.id}: ${e}`);
        process.exit(1);
    }

    if (record.status !== "NEW")
        return;

    if (!created) {
        logger.error(`Deposit record already in DB. Check it manually, id #${record.id}`);
        return;
    }

    // Take fee
    let fee = ethereum.fee().shiftedBy(-token.precision);
    amount = amount.minus(fee).shiftedBy(token.precision);

    let withdrawId = null;
    try {
        // Validate address
        let address = null;
        let re = new RegExp('^(0x[0-9A-Fa-f]+)?$');
        let match = re.exec(payment.memo);
        if (!match) {
            throw new Error('Incorrect memo format');
        } else {
            address = match[1];
        }

        withdrawId = await ethereum.sendEthFromMain(address, amount.toFixed(0));

        if (!withdrawId)
            throw new Error('No tx details');
    } catch (e) {
        logger.error(`Withdraw transfer failed, op #${record.id}: ${e}`);
        record.error = e;
    }

    if (!record.error) {
        logger.info(`Withdraw op #${record.id} successfully processed: ${JSON.stringify(withdrawId)}`);
    } else {
        logger.warn(`Error happened while processing withdraw op #${record.id}`);
        return;
    }

    try {
        await record.update({status: 'DONE', withdrawId: JSON.stringify(withdrawId)});
    } catch (e) {
        logger.error(`Cannot update record #${record.id}`);
        process.exit(1);
    }

    // reserve extra tokens
    //await exchange.checkHotBalance(exchangeId);
};

async function processDeposit(payment) {
    console.log(`New deposit to ${payment.to} (txid: ${payment.txId})`);

    let token = tokens[payment.asset];
    let amount = new BigNumber(payment.amount);
    amount = amount.shiftedBy(-token.precision);

    let record = null;
    let created = null;
    try {

        let status = 'NEW';
        if (amount.isLessThan(tokens[payment.asset].min)) {
            logger.error(`Deposit less than min: ${payment.txId}`);
            status = 'SMALL';
        }

        [record, created] = await models.Deposits.findOrCreate({
            where: {txId: payment.txId}, defaults: {
                txId: payment.txId,
                from: payment.from,
                to: payment.to,
                amount: payment.amount,
                asset: payment.asset,
                blockNumber: payment.blockNumber,
                raw: JSON.stringify(payment.raw)
            }
        });

        if (status !== 'NEW') {
            return;
        }

    } catch (e) {
        logger.error(`Cannot save deposit op #${payment.txId}: ${e}`);
        process.exit(1);
    }

    if (!created) {
        logger.error(`Deposit record already in DB. Check it manually, id #${record.id}`);
        return;
    }

    let address = await models.Addresses.findOne({where: {address: payment.to}});
    if (!address) {
        logger.error(`Unknown deposit address, ${record.id}`);
    }

    let depositId = null;
    try {
        let assetId = token.exchangeId;

        let depositAmount = amount.shiftedBy(token.exchangePrecision);
        let accountId = '1.2.' + String(address.accountId);

        depositId = await exchange.transfer(accountId, depositAmount.toFixed(0), assetId);
        if (!depositId)
            throw new Error('No tx details');
    } catch (e) {
        logger.error(`Deposit transfer failed, op #${record.id}: ${e}`);
        record.depositError = e;
    }

    if (!record.depositError) {
        logger.info(`Deposit op #${record.id} successfully processed: ${JSON.stringify(depositId)}`);
    } else {
        logger.warn(`Error happened while processing deposit op #${record.id}`);
        return;
    }

    try {
        await record.update({depositStatus: 'DONE', depositId: JSON.stringify(depositId)});
    } catch (e) {
        logger.error(`Cannot update record #${record.id}`);
        process.exit(1);
    }
}

const transferDepositToMain = async function(deposit) {
    let deposits = await models.Deposits.findAll({
        where: {
            status: { [Op.in]: ['NEW', 'SMALL'] },
            to: deposit.to,
            asset: deposit.asset
        },
        attributes: ['id', 'amount']
    });

    let depositsBalance = new BigNumber(0);
    let depositsIds = [];
    for (let i = 0; i < deposits.length; i++) {
        console.log('+', deposits[i].amount.toString());
        depositsBalance = depositsBalance.plus(deposits[i].amount.toString());
        depositsIds.push(deposits[i].id);
    }

    let addressBalance = new BigNumber(await ethereum.balance(deposit.to));

    let redeemAmount = null;
    if (addressBalance.isLessThan(depositsBalance)) {
        logger.warn('Address balance less than deposit');
        redeemAmount = addressBalance;
    } else {
        redeemAmount = depositsBalance;
    }

    let address = await models.Addresses.findOne({where: {address: deposit.to}});
    if (!address) {
        logger.error('Unknown deposit address');
        await models.Deposits.update(
            {status: 'BAD'},
            {where: {to: deposit.to}});
        return;
    }

    await models.Deposits.update(
        {status: 'REDEEMING'},
        {where: {id: { [Op.in]: depositsIds }}}
    );

    await ethereum.sendEthToMain(address.accountId, redeemAmount.toFixed(0));

    await models.Deposits.update(
        {status: 'REDEEMED'},
        {where: {id: { [Op.in]: depositsIds }}}
    );
};

const transferDepositsToMain = async function () {

    while (true) {

        let deposit = await models.Deposits.findOne({
            where: {
                status: 'NEW',
            },
            order: ['id']
        });

        if (deposit)
            await transferDepositToMain(deposit);

        if (!deposit)
            await sleep(5 * 1000);
    }
};

const saveLastBlock = async function (block) {
    await lastBlock.update({value: block});
};

const saveLastOp = async function (op) {
    await lastOp.update({value: op});
};

// Loading current state
async function main() {

    exchange = new Exchange(config.exchange);

    ethereum = new Ethereum();
    ethereum.setConfig(config.ethereum);

    try {
        let list = [];
        let addresses = await models.Addresses.findAll();
        for (let i = 0; i < addresses.length; i++) {
            list.push(addresses[i].address);
        }
        addresses = null;
        ethereum.setMonitAddresses(list);
    } catch (e) {
        logger.error('Cannot load addresses');
        process.exit(1);
    }

    try {
        await exchange.connect();
    } catch (e) {
        logger.error('Cannot connect to gate');
        process.exit(1);
    }

    try {
        await ethereum.connect();
    } catch (e) {
        logger.error('Cannot connect to node');
        process.exit(1);
    }

    // Monitoring

    // Ethereum
    try {
        [lastBlock] = await models.States.findOrCreate({where: {name: 'last_block'}});
    } catch (e) {
        logger.error('Cannot findOrCreate last_block property');
        process.exit(1);
    }

    ethereum.on('newIn', (payment) => processDeposit(payment));
    ethereum.on('lastBlock', (block) => saveLastBlock(block));
    ethereum.startMonitoring(lastBlock.value);

    // Exchange
    try {
        [lastOp] = await models.States.findOrCreate({where: {name: 'last_operation'}});
    } catch (e) {
        logger.error('Cannot findOrCreate last_operation property');
        process.exit(1);
    }

    if (!lastOp.value) {
        lastOp.update({value: await exchange.getLastOperation()})
    }

    exchange.on('newIn', (payment) => processWithdraw(payment));
    exchange.on('lastOperation', (op) => saveLastOp(op));
    exchange.start(lastOp.value);

    startApiServer();
    transferDepositsToMain();
}


main();
