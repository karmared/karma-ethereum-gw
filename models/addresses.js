'use strict';
module.exports = (sequelize, DataTypes) => {
    var Addresses = sequelize.define('Addresses', {
        accountId: {
            allowNull: false,
            field: 'account_id',
            type: DataTypes.BIGINT
        },
        address: {
            field: 'address',
            allowNull: false,
            type: DataTypes.STRING
        }
    }, {
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        tableName: 'addresses'
    });
    Addresses.associate = function (models) {
        // associations can be defined here
    };
    return Addresses;
};