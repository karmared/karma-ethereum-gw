const config = require(__dirname + '/../config/config.json');

const Ethereum = require('../src/ethereum');
const BigNumber = require('bignumber.js');

const models = require('../models');
const Op = models.Sequelize.Op;

const logger = require('../src/logger')(require('path').basename(__filename));

let ethereum = null;

var tokens = config.tokens;
var exchangeTokens = {};

for (let name in tokens) {
    if (tokens.hasOwnProperty(name)) {
        exchangeTokens[tokens[name].exchangeId] = name;
    }
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

const main = async function(withdrawId) {
    const record = await models.Withdraws.findOne({where: {id: withdrawId}});

    if (["NEW"].indexOf(record.status) < 0) {
        console.error("Cannot process not NEW withdraws");
        process.exit(1);
    }

    ethereum = new Ethereum();
    ethereum.setConfig(config.ethereum);

    try {
        await ethereum.connect();
    } catch (e) {
        logger.error('Cannot connect to node');
        process.exit(1);
    }

    let exchangeId = record.asset;
    if (!exchangeTokens.hasOwnProperty(exchangeId)) {
        logger.error(`Withdrawing asset ${exchangeId} is not supported`);
        return;
    }
    let token = tokens[exchangeTokens[exchangeId]];
    let amount = new BigNumber(record.amount);
    amount = amount.shiftedBy(-token.exchangePrecision);
    // Take fee
    let fee = ethereum.fee().shiftedBy(-token.precision);
    amount = amount.minus(fee).shiftedBy(token.precision);

    let txId = null;
    try {
        // Validate address
        let address = null;
        let re = new RegExp('^(0x[0-9A-Fa-f]+)?$');
        let match = re.exec(record.memo);
        if (!match) {
            throw new Error('Incorrect memo format');
        } else {
            address = match[1];
        }

        console.log("Send:", address, amount.toFixed(0));
        txId = await ethereum.sendEthFromMain(address, amount.toFixed(0));

        if (!txId)
            throw new Error('No tx details');
    } catch (e) {
        logger.error(`Withdraw transfer failed, op #${record.id}: ${e}`);
        record.error = e;
    }

    if (!record.error) {
        logger.info(`Withdraw op #${record.id} successfully processed: ${JSON.stringify(txId)}`);
    } else {
        logger.warn(`Error happened while processing withdraw op #${record.id}`);
        return;
    }

    try {
        await record.update({status: 'FIX', withdrawId: JSON.stringify(withdrawId)});
    } catch (e) {
        logger.error(`Cannot update record #${record.id}`);
        process.exit(1);
    }

    process.exit(0);
};


const args = process.argv.slice(2);

if (args.length != 1) {
    console.error('Pass withdraw id');
}
let withdrawId = args[0];

main(withdrawId);
